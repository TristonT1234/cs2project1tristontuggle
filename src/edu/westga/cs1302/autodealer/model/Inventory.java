package edu.westga.cs1302.autodealer.model;

import java.util.ArrayList;

import edu.westga.cs1302.autodealer.resources.UI;

/**
 * The Class Inventory.
 * 
 * @author CS1302
 * @version 1.0
 */
public class Inventory {
	
	private ArrayList<Automobile> autos;

	/**
	 * Instantiates a new inventory.
	 * 
	 * @precondition none
	 * @postcondition size() == 0
	 */
	public Inventory() {
		this.autos = new ArrayList<Automobile>();
	}

	/**
	 * Numbers of autos in inventory.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the number of autos in the inventory.
	 */
	public int size() {
		return this.autos.size();
	}

	/**
	 * Adds the auto to the inventory.
	 * 
	 * @precondition auto != null
	 * @postcondition size() == size()@prev + 1
	 *
	 * @param auto the auto
	 * @return true, if add successful
	 */
	public boolean add(Automobile auto) {
		if (auto == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.AUTO_CANNOT_BE_NULL);
		}

		return this.autos.add(auto);
	}

	/**
	 * Adds all the autos to the inventory.
	 * 
	 * @precondition autos != null
	 * @postcondition size() == size()@prev + autos.size()
	 *
	 * @param autos the autos to add to the inventory
	 * 
	 * @return true, if add successful
	 */
	public boolean addAll(ArrayList<Automobile> autos) {
		if (autos == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.AUTOS_CANNOT_BE_NULL);
		}

		return this.autos.addAll(autos);
	}

	/**
	 * Creates an array that holds the count of the number of automobiles in each
	 * year segment starting from minYear to priceSementRange up to the last range
	 * which includes the maxYear car.
	 * 
	 * @precondition yearSegmentRange >= 0
	 * @postcondition none.
	 *
	 * @param yearSegmentRange the year range
	 * @return Array with number of autos in inventory that are in each year
	 *         segment. Returns null if no autos in inventory.
	 */

	public int[] countVehiclesInEachYearSegment(int yearSegmentRange) {

		if (yearSegmentRange < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.RANGE_CANNOT_BE_NEGATIVE);
		}

		if (this.size() == 0) {
			return null;
		}

		int maxYear = this.findYearOfNewestAuto();
		int minYear = this.findYearOfOldestAuto();
		double yearDifference = (double) (maxYear - minYear);

		int segments = (int) Math.ceil(yearDifference / (yearSegmentRange + 1));

		if ((maxYear - minYear) % (yearSegmentRange + 1) == 0) {
			segments++;
		}

		int[] vehicleCount = new int[segments];

		for (Automobile currAuto : this.autos) {
			int currentYear = currAuto.getYear();
			int segmentNumber = (currentYear - minYear) / (yearSegmentRange + 1);
			vehicleCount[segmentNumber]++;
		}

		return vehicleCount;
	}

	/**
	 * Compute average miles of automobiles in inventory.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return average miles; if size() == 0 returns 0
	 */
	public double computeAverageMiles() {
		double sum = 0;

		if (this.autos.size() == 0) {
			return 0;
		}

		for (Automobile currAuto : this.autos) {
			sum += currAuto.getMiles();
		}

		double average = sum / this.autos.size();
		return average;
	}

	/**
	 * Find year of oldest auto.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the year of the oldest auto or Integer.MAX_VALUE if no autos
	 */
	public int findYearOfOldestAuto() {
		int oldest = Integer.MAX_VALUE;

		for (Automobile automobile : this.autos) {
			if (automobile.getYear() < oldest) {
				oldest = automobile.getYear();
			}
		}

		return oldest;
	}

	/**
	 * Find year of newest auto.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the year of the newest auto or Integer.MIN_VALUE if no autos
	 */
	public int findYearOfNewestAuto() {
		int newest = Integer.MIN_VALUE;

		for (Automobile automobile : this.autos) {
			if (automobile.getYear() > newest) {
				newest = automobile.getYear();
			}
		}

		return newest;
	}

	/**
	 * Find most expensive auto in the inventory.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the most expensive automobile or null if no autos in inventory.
	 */
	public Automobile findMostExpensiveAuto() {
		Automobile maxAuto = null;
		double maxPrice = 0;

		for (Automobile currAuto : this.autos) {
			if (currAuto.getPrice() > maxPrice) {
				maxAuto = currAuto;
				maxPrice = maxAuto.getPrice();
			}
		}

		return maxAuto;
	}

	/**
	 * Find least expensive auto in the inventory.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the least expensive automobile or null if no autos in inventory.
	 */
	public Automobile findLeastExpensiveAuto() {
		Automobile minAuto = null;
		double minPrice = Double.MAX_VALUE;

		for (Automobile currAuto : this.autos) {
			if (currAuto.getPrice() < minPrice) {
				minAuto = currAuto;
				minPrice = minAuto.getPrice();
			}
		}

		return minAuto;
	}

	/**
	 * Count autos between specified start and end year inclusive.
	 *
	 * @precondition startYear <= endYear
	 * @postcondition none
	 * 
	 * @param startYear the start year
	 * @param endYear   the end year
	 * @return the number of autos between start and end year inclusive.
	 */
	public int countAutosBetween(int startYear, int endYear) {
		if (startYear > endYear) {
			throw new IllegalArgumentException(UI.ExceptionMessages.START_YEAR_MUST_BE_LESSTHAN_OR_EQUAL_TO_END_YEAR);
		}
		int count = 0;
		for (Automobile automobile : this.autos) {
			if (automobile.getYear() >= startYear && automobile.getYear() <= endYear) {
				count++;
			}
		}

		return count;
	}

	/**
	 * Deletes the specified auto from the inventory.
	 * 
	 * @precondition none
	 * @postcondition if found, size() == size()@prev - 1
	 * @param auto The automobile to delete from the inventory.
	 * 
	 * @return true if the auto was found and deleted from the inventory, false
	 *         otherwise
	 */
	public boolean remove(Automobile auto) {
		return this.autos.remove(auto);
	}

	/**
	 * Gets the autos.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the autos
	 */
	public ArrayList<Automobile> getAutos() {
		return this.autos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "#autos:" + this.size();
	}

}
