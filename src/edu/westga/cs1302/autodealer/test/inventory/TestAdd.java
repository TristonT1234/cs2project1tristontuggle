package edu.westga.cs1302.autodealer.test.inventory;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

class TestAdd {

	@Test
	void testAddNullAuto() {
		Inventory inventory = new Inventory();
		assertThrows(IllegalArgumentException.class, () -> inventory.add(null));
	}
	
	@Test
	void testAddAutoToEmptyInventory() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132, 6900.99));
		assertEquals(1, inventory.size());
	}
	
	@Test
	void testAddMultiplesAutoToInventory() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132, 6900.99));
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132, 6900.99));
		assertEquals(2, inventory.size());
	}

}
