package edu.westga.cs1302.autodealer.test.inventory;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Inventory;

class TestConstruction {
	@Test
	void testDefaultInventory() {
		Inventory inventory = new Inventory();

		assertEquals(0, inventory.size());
	}

}
