package edu.westga.cs1302.autodealer.view.output;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Dealership;
import edu.westga.cs1302.autodealer.model.Inventory;

/**
 * The Class ReportGenerator.
 * 
 * @author CS1302
 * @version 1.0
 */
public class ReportGenerator {
	
	private NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(Locale.US);

	/**
	 * Builds the full summary report of the specified inventory. If inventory is
	 * null, instead of throwing an exception will return a string saying "No
	 * inventory exists.", otherwise builds a summary report of the inventory.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param dealer The dealership to build summary report for
	 *
	 * @return A formatted summary string of the dealership's automobile inventory.
	 */
	public String buildFullSummaryReport(Dealership dealer) {
		String summary = "";
		
		Inventory inventory = dealer.getInventory();

		if (inventory == null) {
			summary = "No inventory exists.";
		} else {
			summary = dealer.getName() + System.lineSeparator();
			summary += "#Automobiles: " + inventory.size() + System.lineSeparator();
		}

		if (inventory.size() > 0) {
			summary += System.lineSeparator();
			summary += this.buildMostAndLeastExpensiveSummaryOutput(inventory);
			
			summary += System.lineSeparator();
			summary += this.buildAverageMilesReport(inventory);
		
			summary += System.lineSeparator();
			summary += this.buildYearSegmentBreakdownSummary(2, inventory);

			summary += System.lineSeparator();

			summary += this.buildYearSegmentBreakdownSummary(4, inventory);
			
			summary += System.lineSeparator();
		}

		return summary;
	}

	private String buildAverageMilesReport(Inventory inventory) {
		String report = "Average miles: ";
		double averageMiles = inventory.computeAverageMiles();
		
		DecimalFormat milesFormat = new DecimalFormat("#,###.000");
		report += milesFormat.format(averageMiles);
		
		return report;
	}

	private String buildMostAndLeastExpensiveSummaryOutput(Inventory inventory) {
		Automobile mostExpensiveAuto = inventory.findMostExpensiveAuto();
		Automobile leastExpensiveAuto = inventory.findLeastExpensiveAuto();

		String report = "Most expensive auto: ";
		report += this.buildIndividualAutomobileReport(mostExpensiveAuto) + System.lineSeparator();
		
		report += "Least expensive auto: ";
		report += this.buildIndividualAutomobileReport(leastExpensiveAuto) + System.lineSeparator();
		
		return report;
	}

	private String buildYearSegmentBreakdownSummary(int yearSegmentRange, Inventory inventory) {

		int[] autoYearSegmentsCount = inventory.countVehiclesInEachYearSegment(yearSegmentRange);
		
		if (autoYearSegmentsCount == null) {
			return "";
		}

		String yearSegmentSummary = "Vehicles in " + yearSegmentRange + " year range segments" + System.lineSeparator();

		int startingYear = inventory.findYearOfOldestAuto();
		
		int endingYear = startingYear + yearSegmentRange;
		
		String stars = "";
		
		if (endingYear > inventory.findYearOfNewestAuto()) {
			endingYear = inventory.findYearOfNewestAuto();
		}
		
		for (int index = 0; index < autoYearSegmentsCount.length; index++) {
			
			if (autoYearSegmentsCount[index] == 0) {
				stars = "0";
			} else if (autoYearSegmentsCount[index] > 0) {
				for (int counter = 0; counter < autoYearSegmentsCount[index]; counter ++  ) {
					stars += "*";
				}
			}
			
			yearSegmentSummary += startingYear + " - " + endingYear + " : " + stars + System.lineSeparator();
			startingYear = endingYear + 1;
			 
			if (startingYear + yearSegmentRange > inventory.findYearOfNewestAuto()) {
				endingYear = inventory.findYearOfNewestAuto();
			} else {
				endingYear = startingYear + yearSegmentRange;
			}
			
		}
		
		return yearSegmentSummary;
	}

	private String buildIndividualAutomobileReport(Automobile auto) {
		String output = this.currencyFormatter.format(auto.getPrice()) + " " + auto.getYear() + " " + auto.getMake() + " "
				+ auto.getModel();
		return output;
	}

}
