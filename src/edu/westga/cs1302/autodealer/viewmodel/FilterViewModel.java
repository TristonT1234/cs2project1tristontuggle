
package edu.westga.cs1302.autodealer.viewmodel;

import edu.westga.cs1302.autodealer.model.Dealership;
import edu.westga.cs1302.autodealer.model.DealershipGroup;
import edu.westga.cs1302.autodealer.resources.UI;
import edu.westga.cs1302.autodealer.view.output.ReportGenerator;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;

import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;

/**
 * The Class FilterViewModel.
 *
 * @author CS1302
 * @version 1.0
 */
public class FilterViewModel {
	private ReportGenerator report;
	private DealershipGroup dealers;

	private ListProperty<Dealership> dealersProperty;
	private ObjectProperty<Dealership> selectedDealershipProperty;

	private StringProperty makeProperty;
	private StringProperty minYearProperty;
	private StringProperty maxYearProperty;

	private StringProperty summaryProperty;

	/**
	 * Instantiates a new filter view model.
	 *
	 * @precondition dealers != null
	 * @postcondition none
	 * 
	 * @param dealers the dealers
	 */
	public FilterViewModel(DealershipGroup dealers) {
		if (dealers == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.DEALERS_CANNOT_BE_NULL);
		}

		this.dealers = dealers;
		this.report = new ReportGenerator();

		this.makeProperty = new SimpleStringProperty();
		this.minYearProperty = new SimpleStringProperty();
		this.maxYearProperty = new SimpleStringProperty();

		this.selectedDealershipProperty = new SimpleObjectProperty<Dealership>();
		this.dealersProperty = new SimpleListProperty<Dealership>(
				FXCollections.observableArrayList(this.dealers.getDealers()));

		this.summaryProperty = new SimpleStringProperty();
	}

	/**
	 * Update dealerships.
	 */
	public void updateDealerships() {
		this.dealersProperty.set(FXCollections.observableArrayList(this.dealers.getDealers()));
	}

	/**
	 * Update summary report.
	 *
	 * @precondition none
	 * @postcondition none
	 */
	public void updateSummary() {
		//		String make = this.makeProperty.get();
		//		Dealership selectedDealer = this.selectedDealershipProperty.get();

		String summaryReport = this.report.buildFullSummaryReport(this.selectedDealershipProperty.get());
		this.summaryProperty.set(summaryReport);
	}

	/**
	 * Gets the dealers property.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the dealersProperty
	 */
	public ListProperty<Dealership> dealersProperty() {
		return this.dealersProperty;
	}

	/**
	 * Gets the selected dealership property.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the selectedDealershipProperty
	 */
	public ObjectProperty<Dealership> selectedDealershipProperty() {
		return this.selectedDealershipProperty;
	}

	/**
	 * Gets the make property.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the makeProperty
	 */
	public StringProperty makeProperty() {
		return this.makeProperty;
	}

	/**
	 * Gets the min year property.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the minYearProperty
	 */
	public StringProperty minYearProperty() {
		return this.minYearProperty;
	}

	/**
	 * Gets the max year property.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the maxYearProperty
	 */
	public StringProperty maxYearProperty() {
		return this.maxYearProperty;
	}

	/**
	 * Summary property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the summary property
	 */
	public StringProperty summaryProperty() {
		return this.summaryProperty;
	}

}
